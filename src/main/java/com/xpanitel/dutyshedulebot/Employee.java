package com.xpanitel.dutyshedulebot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author volkov
 */
public class Employee {

    private String username;
    private long chatId;

    private boolean beOnDuty;       // Дежурит
    private boolean healthy;        // Здоров
    private boolean onLeave;        // в отпуске
    /**
     * График задается переменной int. 1 - обычный 5/2 2 - график 2/2 (б) 3 -
     * график 2/2 (ч) 4 - 5/2 с понедельным дежурством в офисе
     */
    private int schedule;           // вид графика
    private int shiftCounter;       // Счечик смен

    /**
     * Состояние задается переменной int. 0 - обычное состояние 1 - режим
     * настроек 2 - режим создания графика 3 - режим подтверждения смены
     */
    private int condition;
    
    private ArrayList<Calendar> officeShifts = new ArrayList<>();

    private static ArrayList<Employee> employees = new ArrayList<>();

    private Employee(String username, long chatId) {
        this.username = username;
        this.chatId = chatId;
        this.beOnDuty = false;
        this.healthy = true;
        this.onLeave = false;
        this.schedule = 1;
        this.shiftCounter = 0;
        this.condition = 0;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
        saveEmployees();
    }

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
        saveEmployees();
    }

    /**
     * Обращение к параметру может ли сотрудник дежурить
     *
     * @return true - если может дежурить, false - если не может
     */
    public boolean isBeOnDuty() {
        return beOnDuty;
    }

    /**
     * Задает значение параметра может ли сотрудник дежурить
     *
     * @param beOnDuty true - если может дежурить, false - если не может
     */
    public void setBeOnDuty(boolean beOnDuty) {
        this.beOnDuty = beOnDuty;
        saveEmployees();
    }

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
        saveEmployees();
    }

    public boolean isOnLeave() {
        return onLeave;
    }

    public void setOnLeave(boolean onLeave) {
        this.onLeave = onLeave;
        saveEmployees();
    }

    /**
     * График задается переменной int. 1 - обычный 5/2 2 - график 2/2 (б) 3 -
     * график 2/2 (ч) 4 - 5/2 с понедельным дежурством в офисе
     */
    public int getSchedule() {
        return schedule;
    }

    /**
     * График задается переменной int. 1 - обычный 5/2 2 - график 2/2 (б) 3 -
     * график 2/2 (ч) 4 - 5/2 с понедельным дежурством в офисе
     */
    public void setSchedule(int schedule) {
        this.schedule = schedule;
        saveEmployees();
    }

    public int getShiftCounter() {
        return shiftCounter;
    }

    public void setShiftCounter(int shiftCounter) {
        this.shiftCounter = shiftCounter;
        saveEmployees();
    }

    /**
     * Состояние задается переменной int. 0 - обычное состояние 1 - режим
     * настроек 2 - режим создания графика 3 - режим подтверждения смены
     */
    public int getCondition() {
        return condition;
    }

    /**
     * Состояние задается переменной int. 0 - обычное состояние 1 - режим
     * настроек 2 - режим создания графика 3 - режим подтверждения смены
     */
    public void setCondition(int condition) {
        this.condition = condition;
        saveEmployees();
    }

    /**
     * Добавлет смену в счетчик смен
     *
     * @return новое колличество смен
     */
    public int insertShiftCounter() {
        shiftCounter++;
        saveEmployees();
        return shiftCounter;
    }
    
    @Deprecated
    public static Employee newEmployee(String username, long chatId) {
        Employee employee = new Employee(username, chatId);
        employees.add(employee);
        saveEmployees();
        return employee;
    }
    
    public static void addEmployee (Employee employee) {
        employees.add(employee);
        saveEmployees();
    }

    public static Employee newUnaproveEmployee(String username, long chatId) {
        Employee employee = new Employee(username, chatId);
        saveEmployees();
        return employee;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Employee) {
            Employee e = (Employee) obj;
            if (e.getChatId() == getChatId()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Сохраняет массив сотрудников в виде файла конфига
     */
    private static void saveEmployees() {
        try {
            File file = new File("employees");
            if (!file.exists() || file.isDirectory()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file, false);
            for (Employee e : employees) {
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(e.getUsername());
                stringBuffer.append(";");
                stringBuffer.append(e.getChatId());
                stringBuffer.append(";");
                stringBuffer.append(e.isBeOnDuty());
                stringBuffer.append(";");
                stringBuffer.append(e.isHealthy());
                stringBuffer.append(";");
                stringBuffer.append(e.isOnLeave());
                stringBuffer.append(";");
                stringBuffer.append(e.getSchedule());
                stringBuffer.append(";");
                stringBuffer.append(e.getShiftCounter());
                stringBuffer.append(";");
                stringBuffer.append(e.getCondition());

                fw.append(stringBuffer.toString());
                fw.append(System.getProperty("line.separator"));
            }
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Чтение файла employees и восстановление из него списка сотрудников
     *
     */
    private static void readEmployees() {
        try {
            File file = new File("employees");
            if (!file.exists() || file.isDirectory()) {
                
                System.out.println("файл не найден");
                
                return;
            }
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                
                String line = scanner.nextLine();
                String[] strings = line.split(";");
                
                if (strings.length == 8) {
                    Employee e = new Employee(strings[0], Long.parseLong(strings[1]));
                    e.schedule = (Integer.parseInt(strings[5]));
                    e.shiftCounter = (Integer.parseInt(strings[6]));
                    e.condition = (Integer.parseInt(strings[7]));
                    if (strings[2].equalsIgnoreCase("true")) {
                        e.beOnDuty = (true);
                    } else {
                        e.beOnDuty = (false);
                    }
                    if (strings[3].equalsIgnoreCase("true")) {
                        e.healthy = (true);
                    } else {
                        e.healthy = (false);
                    }
                    if (strings[4].equalsIgnoreCase("true")) {
                        e.onLeave = (true);
                    } else {
                        e.onLeave = (false);
                    }
                    employees.add(e);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Первичная инициализация списка сотрудников
     */
    public static void init() {
        readEmployees();
    }
    
    /**
     * Поиск сотрудника по Id
     * @param chatId ChatId сотрудника которого ищем 
     * @return Возвращает сотруника с заданным chatId, либо null если такого нет в списке
     */
    public static Employee getEmployee(long chatId){
        for (Employee e : employees) {
            if (e.getChatId() == chatId) {
                return e;
            }
        }
        return null;
    }
    
    /**
     * Проверка, есть ли сотрудник с заданным chatId. 
     * @param chatId chatId - выбранного сотрудника
     * @return возвращает true - если такой сотрудник есть, false - если такого сотрудника нет
     */
    public static boolean isEmployee(long chatId) {
        for (Employee e : employees) if (e.getChatId() == chatId) return true;
        return false;
    }
    
    /**
     * Получение списка сотрудников, которые могут дежурить
     * @return Возвращает ArrayList сотрудников, которые могут дежурить
     */
    public static ArrayList<Employee> beOnDutyEmployees() {
        ArrayList<Employee> list = new ArrayList<>();
        for (Employee e : employees) if (e.beOnDuty) list.add(e);
        return list;
    }
    
    /**
     * Привести смены в офисе в начальное положение
     */
    public void clearOfficeShifts() {
        officeShifts.clear();
        Calendar calendar = new GregorianCalendar();
        int monday = 9 - (calendar.get(Calendar.DAY_OF_WEEK));
        calendar.add(Calendar.DATE, monday);
        for (int i = 0; i < 5; i++) {
            Calendar c = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            officeShifts.add(c);
            calendar.add(Calendar.DATE, 1);
        }
    }
    
    /**
     * Убираем смену из списка.
     * @param date дата, которую надо удалить из списка доступных смен
     */
    public void removeFromOfficeShift(Calendar date) {
        Iterator<Calendar> i = officeShifts.iterator();
        while(i.hasNext()) 
            if (i.next().get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR))
                i.remove();
    }
    
    /**
     * Проверяем есть ли выбранная дата в списке данного сотрудника
     * @param date проверяемая дата
     * @return возвращает true - если этот день есть в списке, false - если его нет
     */
    public boolean isOfficeShift(Calendar date) {
        boolean b = false;
        for (Calendar c : officeShifts) 
            if (c.get(Calendar.DAY_OF_YEAR) == date.get(Calendar.DAY_OF_YEAR)) 
                b = true;
        return b;
    }
    
    /**
     * Выводит массив дат из массива смен в офисе. Даты выводятся в формате dd.mm
     * @return Возвращает массив строк - даты, сотрудника в офисе
     */
    public ArrayList<String> getOfficeShifts() {
        ArrayList<String> list = new ArrayList<>();
        for (Calendar c : officeShifts) {
            list.add(new String(c.get(Calendar.DAY_OF_MONTH) + "." + (1 + c.get(Calendar.MONTH))));
        }
        return list;
    }
}
