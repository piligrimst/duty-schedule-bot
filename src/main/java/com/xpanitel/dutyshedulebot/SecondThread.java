package com.xpanitel.dutyshedulebot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author volkov
 */
public class SecondThread extends Thread {

    /*
      Конфигурация бота по построению графиков и оповещениям
     */
    private int DAY_SCHEDULING = 2;
    private int HOUR_SCHEDULING = 23;
    private int IS_SHEDULING = 0;
    private ArrayList<Employee> beOnDuty = new ArrayList<>();
    
    private static final int DAY_CLEARPARAMETERS = 7;
    private static final int HOUR_CLEARPARAMETERS = 10;
    

    /*
      Основные поля класса
     */
    private final Bot bot;

    public SecondThread(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void run() {
        while (true) {

            /*
              Для начала получаем текущее время
              для последующей работы с ним
             */
            Calendar now = new GregorianCalendar();

            /*
              Еженедельные действия:
              - составление графика
             */
            if (IS_SHEDULING == 0) {
                if (now.get(Calendar.DAY_OF_WEEK) == DAY_SCHEDULING) {
                    if (now.get(Calendar.HOUR_OF_DAY) == HOUR_SCHEDULING) {
                        beOnDuty = Employee.beOnDutyEmployees();
                        for (Employee e : beOnDuty) {
                            bot.sheduling(e);
                        }
                        IS_SHEDULING = 1;
                    }

                }
            }
            /*
              Еженедельные действия:
              - составление графика спустя час или крики в чаты
              - составление графика спустя 2 часа или крики в чаты
             */
            if(IS_SHEDULING == 1) {
                if (now.get(Calendar.DAY_OF_WEEK) == DAY_SCHEDULING) {
                    if (now.get(Calendar.HOUR_OF_DAY) == (HOUR_SCHEDULING + 1)) {
                        boolean flag = false;
                        ArrayList<String> userNames = new ArrayList<>();
                        for (Employee e : beOnDuty) {
                            if (e.getCondition() == 2) flag = true;
                            userNames.add("@" + e.getUsername());
                        }
                        if (flag) {
                            StringBuilder textMessage = new StringBuilder();
                            textMessage.append("Некоторые сотрудники не выбрали рабочие дни на следующую неделю: ");
                            for (String s : userNames) textMessage.append(s + " ");
                            bot.sendMessage(bot.getAdminChatId(), textMessage.toString(), bot.homeKeyboard());
                            if (bot.getFirstGroupChatId() != 0) bot.sendMessage(bot.getFirstGroupChatId(), textMessage.toString());
                            IS_SHEDULING = 3;
                        } else {
                            WorkShift.formAShifts();
                        }
                    } else if (now.get(Calendar.HOUR_OF_DAY) == (HOUR_SCHEDULING + 2)) {
                        boolean flag = false;
                        ArrayList<String> userNames = new ArrayList<>();
                        for (Employee e : beOnDuty) {
                            if (e.getCondition() == 2) flag = false;
                            userNames.add("@" + e.getUsername());
                        }
                        if (flag) {
                            StringBuilder textMessage = new StringBuilder();
                            textMessage.append("Некоторые сотрудники не выбрали рабочие дни на следующую неделю: ");
                            for (String s : userNames) textMessage.append(s + " ");
                            bot.sendMessage(bot.getAdminChatId(), textMessage.toString(), bot.homeKeyboard());
                            if (bot.getFirstGroupChatId() != 0) bot.sendMessage(bot.getFirstGroupChatId(), textMessage.toString());
                            if (bot.getSecondGroupChatId()!= 0) bot.sendMessage(bot.getSecondGroupChatId(), "Я не могу составить график на следующую неделю, так как некоторые сотрудники мне не отвечают!");
                            IS_SHEDULING = 3;
                        } else {
                            WorkShift.formAShifts();
                        }
                    }
                }
            }
            
            
            /*
              Еженедельные действия:
              - составление графика
             */
            if (now.get(Calendar.DAY_OF_WEEK) == DAY_CLEARPARAMETERS) {
                if (now.get(Calendar.HOUR_OF_DAY) == 10) {
                    clearParameters();
                }
            }

        }
    }
    
    private void clearParameters() {
        IS_SHEDULING = 0;
    }

}
