/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xpanitel.dutyshedulebot;

import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author volkov
 */
public class WorkShift {

    private final Calendar DATE;
    private ArrayList<Employee> arrayEmployee = new ArrayList<Employee>();
    private static ArrayList<Employee> arrayEmployeeForForm = new ArrayList<>();

    private WorkShift(Calendar date) {
        this.DATE = date;
    }

    /**
     * Возвращает колличество сотрудников в смене
     *
     * @return колличество сотрудников в смене
     */
    public int size() {
        return arrayEmployee.size();
    }

    /**
     * Добавляет сотрудника в смену
     *
     * @param employee добавляемый сотрудника в массив
     */
    public void add(Employee employee) {
        arrayEmployee.add(employee);
    }

    /**
     * Удяляет сотрудника из смены.
     *
     * @param i ИД сотрудника в массиве в смене
     * @return Возвращает удаленного сотрудника
     */
    public Employee remove(int i) {
        return arrayEmployee.remove(i);
    }

    /**
     * Отдает сотрудника, который работает в эту смену
     *
     * @param i номер сотрудника в масиве
     * @return Сотрудник под нужным номером
     */
    public Employee get(int i) {
        return arrayEmployee.get(i);
    }

    /**
     * Заменяет сотрудника в смене на другого сотрудника
     *
     * @param removedEmployee сотрудник которого надо удалить из смены
     * @param addedEmployee сотрудник которого добавляют в смену
     * @return Возвращает true, если сотрудник найден и заменен, false - если не
     * удалось найти и заменить сотрудника
     */
    public boolean replace(Employee removedEmployee, Employee addedEmployee) {
        for (int i = 0; i < size(); i++) {
            if (get(i).equals(removedEmployee)) {
                remove(i);
                add(addedEmployee);
                return true;
            }
        }
        return false;
    }

    /**
     * Формирование смены. Формирование смены происходит из списка людей, кто
     * готов работать в этот день. В смену попадают те, кто отработал меньше
     * всего смен в текущем месяе
     *
     * @param date дата на которую формируется смена
     * @param arrayEmployee Массив сотрудников, кто может работать в данную дату
     * @param persons число людей в смене на эту дату
     * @return Возвращает сформированную смену
     * @throws Exception Выбрасывает исключение, если число людей готовых
     * работать в этот ден, меньше необходимого числа людей в смену
     */
    public WorkShift formAShift(Calendar date, ArrayList<Employee> arrayEmployee, int persons) throws Exception {
        if (arrayEmployee.size() < persons) {
            throw new Exception("Недостаточно людей для формирования смены");
        }
        WorkShift shift = new WorkShift(date);
        for (int i = persons; i == 0; i--) {
            int shifts = 365;
            Employee employee = null;
            for (Employee e : arrayEmployee) {
                if (e.getShiftCounter() <= shifts) {
                    employee = e;
                    shifts = e.getShiftCounter();
                }
            }
            employee.insertShiftCounter();
            shift.add(employee);
        }

        return shift;
    }

    static void formAShifts() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Создание массива с сотрудниками для формирование смен
     *
     * @param e сотрудник выбравший дни для работы из офиса
     */
    public static void addEmployeeFromFormatShift(Employee e) {
        arrayEmployeeForForm.add(e);
    }

    /**
     * Очистка списка сотрудников для формирования смены.
     */
    public static void clearArrayFromFormatShift() {
        arrayEmployeeForForm.clear();
    }
}
