/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xpanitel.dutyshedulebot;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

/**
 *
 * @author volkov
 */
public class Bot extends TelegramLongPollingBot {

    private final String BOT_TOKEN;
    private final String BOT_USERNAME;

    /*
      Переменные инициализации бота
      И его конфигурации
     */
    private boolean botInitial;
    private String adminUsername;
    private long adminChatId = 0;
    private long firstGroupChatId = 0;
    private String firstGroupChatName;
    private long secondGroupChatId = 0;
    private String secondGroupChatName;
    private boolean silenceMode = false;

    /*
      Массив сотрудников и смен при работе бота
     */
    private ArrayList<WorkShift> workShifts = new ArrayList<>(); // Список смен
    private HashMap<Integer, Employee> forApprove = new HashMap<>(); // Сотрудник на подтверждение

    /*
      Поля для работы с клавиатурой
     */
    private ReplyKeyboardMarkup rkm = new ReplyKeyboardMarkup();
    private KeyboardRow keyboardRowFirst = new KeyboardRow();
    private KeyboardRow keyboardRowSecond = new KeyboardRow();
    private KeyboardRow keyboardRowThird = new KeyboardRow();
    private ArrayList<KeyboardRow> arrayKeyboardRows = new ArrayList<KeyboardRow>();

    /*
      Ключевые слова при работе с ботом
     */
    private static final String WORD_START = "/start";
    private static final String WORD_SETTINGS = "Настройки";
    private static final String WORD_ENABLE_DUTY = "Дежурить в офисе";
    private static final String WORD_DISABLE_DUTY = "Не дежурить";
    private static final String WORD_SHEDULE_5_2_RANDOM = "5/2 сменами";
    private static final String WORD_SHEDULE_2_2_1 = "2/2 белая";
    private static final String WORD_SHEDULE_2_2_2 = "2/2 черная";
    private static final String WORD_SHEDULE_5_2_WEEK = "5/2 неделями";
    private static final String WORD_HOME = "Вернуться";
    private static final String WORD_DONE = "Готово";
    
    /*
      Сообщения от бота
    */
    private static final String MSG_SHEDULING = "Выбери даты, в которые на следующей неделе хочешь работать на удаленке";

    private Bot(String BOT_TOKEN, String BOT_USERNAME) throws Exception {
        this.BOT_TOKEN = BOT_TOKEN;
        this.BOT_USERNAME = BOT_USERNAME;
        this.botInitial = initialBot();
    }

    public long getAdminChatId() {
        return adminChatId;
    }

    public long getFirstGroupChatId() {
        return firstGroupChatId;
    }

    public long getSecondGroupChatId() {
        return secondGroupChatId;
    }

    @Override
    public String getBotToken() {
        return BOT_TOKEN;
    }

    @Override
    public String getBotUsername() {
        return BOT_USERNAME;
    }

    @Override
    public void onUpdateReceived(Update update) {
        /*
        Разбираем пришедшее сообщение на составляющие
         */
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        String userName = update.getMessage().getChat().getUserName();
        String name = new StringBuilder()
                .append(update.getMessage().getChat().getFirstName())
                .append(" ")
                .append(update.getMessage().getChat().getLastName())
                .toString();
        /*
          Очищаем клавиатуру
         */
        cleanKeyboard();

        /*
        Пооверяем известер ли ChatId администратора бота
        Если не известен, отвечаем только администратору и сохраняем его chatId
        Вносим соответствующие коррективы в botsettings.conf
         */
        if (!botInitial) {
            if (userName.equalsIgnoreCase(adminUsername)) {
                adminChatId = chatId;
                botInitial = true;
                Employee.addEmployee(Employee.newUnaproveEmployee(userName, chatId));
                sendMessage(chatId, "Приступим!");
                savebotconfig();
            }
            return;
        }

        if (text.equalsIgnoreCase(WORD_START)) {
            if (!silenceMode) {
                if (chatId > 0) {
                    /*
                      Проверяем, является ли пользователь сотрудников
                      Если пользователь уже сотрудник, игнорим
                      Иначе - запрашиваем апрув
                     */
                    if (Employee.isEmployee(chatId)) {
                        return;
                    }
                    Employee unapprove = Employee.newUnaproveEmployee(userName, chatId);
                    String textMessage;
                    if (userName != null) {
                        textMessage = "Пользователь с UserName @" + userName + " добавился в бота\n"
                                + "Если это сотрудник - ответь на это сообщение \"+\" и его добавит в список сотрудников\n"
                                + "Если он не сотрудник - ответь на это сообщение \"-\" и его исключат из списка ожидания";
                    } else {
                        textMessage = "Пользователь " + name + " добавился в бота\n"
                                + "Если это сотрудник - ответь на это сообщение \"+\" и его добавит в список сотрудников\n"
                                + "Если он не сотрудник - ответь на это сообщение \"-\" и его исключат из списка ожидания";
                    }
                    forApprove.put(sendMessage(adminChatId, textMessage), unapprove);
                    return;
                } else {
                    /*
                      Если сообщение пришло из рупового чата, проверяем является ли он рабочим
                      Если заданное админом имя соответствует - добавляем его чатИд
                     */
                    if (chatId != firstGroupChatId && chatId != secondGroupChatId) {
                        if (userName.equalsIgnoreCase(firstGroupChatName)) {
                            firstGroupChatId = chatId;
                        }
                        if (userName.equalsIgnoreCase(secondGroupChatName)) {
                            secondGroupChatId = chatId;
                        }
                    }
                }
                return;
            }
        }

        /*
         Система подтверждения сотрудника.
         Админ должен отправить "+" или "-" на сообщение о том, что новый 
         сотрудник хочет добавится в чат. "+" - добавит сотрудника, "-" - отказ
         */
        if (chatId == adminChatId && text.equalsIgnoreCase("+") && update.getMessage().isReply()) {
            Employee e = forApprove.remove(update.getMessage().getReplyToMessage().getMessageId());
            Employee.addEmployee(e);
            keyboardRowFirst.add(WORD_SETTINGS);
            arrayKeyboardRows.add(keyboardRowFirst);
            rkm.setKeyboard(arrayKeyboardRows);
            sendMessage(e.getChatId(), "Добавлен!", rkm);
            return;
        }
        if (chatId == adminChatId && text.equalsIgnoreCase("-") && update.getMessage().isReply()) {
            forApprove.remove(update.getMessage().getReplyToMessage().getMessageId());
            return;
        }

        /*
          Переход в режим настроек
         */
        if (text.equalsIgnoreCase(WORD_SETTINGS)) {
            if (Employee.isEmployee(chatId)) {
                Employee e = Employee.getEmployee(chatId);
                if (e.getCondition() == 0) {
                    e.setCondition(1);
                    sendMessage(chatId, settingsEmployee(e), rkm);
                }
            }
            return;
        }
        
        /*
          Включение работы в офисе
        */
        if (text.equalsIgnoreCase(WORD_ENABLE_DUTY) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e.getCondition() == 1) {
                e.setBeOnDuty(true);
                sendMessage(chatId, settingsEmployee(e), rkm);
            }
            return;
        }
        
        /*
          Выключение работы в офисе
        */
        if (text.equalsIgnoreCase(WORD_DISABLE_DUTY) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e.getCondition() == 1) {
                e.setBeOnDuty(false);
                e.setCondition(0);
                sendMessage(chatId, "Текущие настройки:\n"
                        + "Не дежуришь в офисе", homeKeyboard());
            }
            return;
        } 
        
        /*
          Смена графика
        */
        if (text.equalsIgnoreCase(WORD_SHEDULE_2_2_1) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e. getCondition() == 1 && e.isBeOnDuty()) {
                e.setSchedule(2);
                sendMessage(chatId, settingsEmployee(e), rkm);
            }
            return;
        }
        if (text.equalsIgnoreCase(WORD_SHEDULE_2_2_2) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e. getCondition() == 1 && e.isBeOnDuty()) {
                e.setSchedule(3);
                sendMessage(chatId, settingsEmployee(e), rkm);
            }
            return;
        }
        if (text.equalsIgnoreCase(WORD_SHEDULE_5_2_RANDOM) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e. getCondition() == 1 && e.isBeOnDuty()) {
                e.setSchedule(1);
                sendMessage(chatId, settingsEmployee(e), rkm);
            }
            return;
        }
        if (text.equalsIgnoreCase(WORD_SHEDULE_5_2_WEEK) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e. getCondition() == 1 && e.isBeOnDuty()) {
                e.setSchedule(4);
                sendMessage(chatId, settingsEmployee(e), rkm);
            }
            return;
        }
        
        /*
          Возвращение на главную
        */
        if (text.equalsIgnoreCase(WORD_HOME) && Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e.isBeOnDuty()) {
                e.setCondition(0);
                sendMessage(chatId, "На главной", homeKeyboard());
            }
            return;
        }
        
        /*
          Формирование графика на неделю
        */
        if (Employee.isEmployee(chatId)) {
            Employee e = Employee.getEmployee(chatId);
            if (e.getCondition() == 2) {
                /*
                  Завершение выбора дней
                */
                if (text.equalsIgnoreCase(WORD_DONE)) {
                    e.setCondition(0);
                    WorkShift.addEmployeeFromFormatShift(e);
                    sendMessage(chatId, "Спасибо. Твои пожелания будут учтены", homeKeyboard());
                    return;
                }
                System.out.println(text);
                String[] dates = text.split("\\.");
                System.out.println(dates.length);
                if (dates.length == 2) {
                    int year = new GregorianCalendar().get(Calendar.YEAR);
                    int month = Integer.parseInt(dates[1]) - 1;
                    int day = Integer.parseInt(dates[0]);
                    Calendar c = new GregorianCalendar(year, month, day);
                    if (e.isOfficeShift(c)) {
                        e.removeFromOfficeShift(c);
                    }
                }
                
                sheduling(e);
                return;
                
            }
        }
        

    }

    /**
     * Инициализация бота. Создает класс бота и запускает второй поток, 
     * передавая ему ссылку на созданный объек бота
     *
     * @param BOT_TOKEN Токен бота от BotFather для инициализации
     * @param BOT_USERNAME Юзернэйм бота от BotFather для инициализации
     * @return Возвращает ссылку на созданный объект бота
     * @throws Exception Возвращает исключение, если UserName администратора
     * бота не указан в файле конфигурации
     */
    public static Bot init(String BOT_TOKEN, String BOT_USERNAME) throws Exception {
        Bot bot = new Bot(BOT_TOKEN, BOT_USERNAME);
        SecondThread thread = new SecondThread(bot);
        thread.start();
        return bot;
    }

    /**
     * Инициализация бота. Запускается при создании бота и считывает конфиг файл
     * botsettings.conf. Пасит его и заполняет поля того кто админ и с какими
     * групповыми чатами буде работать бот. Файл не может быть пустым, но имеет
     * 1 обязательный параметр - adminUsername, определяющий, UserName админа
     * бота
     *
     * @return Возвращает true - если из файла получен ChatId админа, fales -
     * если не получен
     * @throws Exception Возвращает исключение, если UserName администратора
     * бота не указан в файле конфигурации
     */
    private boolean initialBot() throws Exception {
        try {
            FileReader fileReader = new FileReader("botsettings.conf");
            Scanner scanner = new Scanner(fileReader);
            String nextLine;
            String[] params;

            while (scanner.hasNext()) {
                nextLine = scanner.nextLine();
                params = nextLine.split(" : ");
                if (params.length == 2) {
                    switch (params[0]) {
                        case "adminUsername" ->
                            adminUsername = params[1];
                        case "adminChatId" ->
                            adminChatId = Long.parseLong(params[1]);
                        case "firstGroupChatId" ->
                            firstGroupChatId = Long.parseLong(params[1]);
                        case "firstGroupChatName" ->
                            firstGroupChatName = params[1];
                        case "secondGroupChatId" ->
                            secondGroupChatId = Long.parseLong(params[1]);
                        case "secondGroupChatName" ->
                            secondGroupChatName = params[1];
                    }
                }
            }

            fileReader.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (adminUsername.isEmpty()) {
            throw new Exception("В файле botsettings.conf не указан, либо не правильно указан параметр adminUsername");
        }
        Employee.init();
        return adminChatId != 0;
    }

    /**
     * Отправка сообщение ботом.
     *
     * @param chatId id чата в который отправляетя сообщение
     * @param textMessage Текст отправляемого сообщения
     */
    public Integer sendMessage(long chatId, String textMessage) {
        SendMessage message = new SendMessage(Long.toString(chatId), textMessage);
        try {
            return execute(message).getMessageId();
        } catch (TelegramApiException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Отправка сообщения с клавиатурой ботом
     *
     * @param chatId id чата в который отправляетя сообщение
     * @param textMessage Текст отправляемого сообщения
     * @param keyboard Клавиатура отправляемая сообщением
     */
    public Integer sendMessage(long chatId, String textMessage, ReplyKeyboardMarkup keyboard) {
        keyboard.setSelective(Boolean.TRUE);
        keyboard.setResizeKeyboard(Boolean.TRUE);
        keyboard.setOneTimeKeyboard(Boolean.TRUE);

        SendMessage message = new SendMessage(Long.toString(chatId), textMessage);
        message.setReplyMarkup(keyboard);
        try {
            return execute(message).getMessageId();
        } catch (TelegramApiException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Схранение конфигурации бота в файл. Необходимо вызывать метод при каждом
     * внесении изменеий
     */
    private void savebotconfig() {
        try {
            FileWriter fileWriter = new FileWriter("botsettings.conf", false);
            fileWriter.append("adminUsername : " + adminUsername);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.append("adminChatId : " + adminChatId);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.append("firstGroupChatId : " + firstGroupChatId);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.append("firstGroupChatName : " + firstGroupChatName);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.append("secondGroupChatId : " + secondGroupChatId);
            fileWriter.append(System.getProperty("line.separator"));
            fileWriter.append("secondGroupChatName : " + secondGroupChatName);
            fileWriter.append(System.getProperty("line.separator"));

            fileWriter.close();
        } catch (IOException ex) {
            Logger.getLogger(Bot.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Очистка содержимого клавиатуры. При вызове данного метода, очищает строки
     * клавиатуры и массив этих строк
     */
    private void cleanKeyboard() {
        arrayKeyboardRows.clear();
        keyboardRowFirst.clear();
        keyboardRowSecond.clear();
        keyboardRowThird.clear();
    }

    /**
     * Собирает клавиатуру для ответа
     *
     * @param keyboardRows Принимает на вход строки клавиатур
     * @return Возвращает готовую для отправки клавиатуру
     */
    private ReplyKeyboardMarkup createReplyKeyboard(KeyboardRow... keyboardRows) {
        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        for (KeyboardRow row : keyboardRows) {
            arrayKeyboardRows.add(row);
            keyboardMarkup.setKeyboard(arrayKeyboardRows);
        }
        return keyboardMarkup;
    }

    /**
     * Создание ответного текста с настройками и кнопками
     *
     * @param e Сотрудник с чъими настроками мы работаем
     * @return Возвращает текст сообщения для сотрудника с настройками
     */
    private String settingsEmployee(Employee e) {
        StringBuffer buffer = new StringBuffer("Текущие настройки: \n");
        if (e.isBeOnDuty()) {
            keyboardRowFirst.add(WORD_DISABLE_DUTY);
            buffer.append("Можешь дежурить в офисе\n");
            buffer.append("График работы: ");
            switch (e.getSchedule()) {
                case 1:
                    buffer.append("5/2 со сменным дежурством\n");
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_1);
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_2);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_WEEK);
                    break;
                case 2:
                    buffer.append("2/2 белая\n");
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_2);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_RANDOM);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_WEEK);
                    break;
                case 3:
                    buffer.append("2/2 черная\n");
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_1);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_RANDOM);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_WEEK);
                    break;
                case 4:
                    buffer.append("5/2 с понедельным дежурством\n");
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_1);
                    keyboardRowSecond.add(WORD_SHEDULE_2_2_2);
                    keyboardRowSecond.add(WORD_SHEDULE_5_2_RANDOM);
                    break;
            }
            buffer.append("Ты отработал ");
            buffer.append(e.getShiftCounter());
            buffer.append(" смен в этом месяце");
            
            keyboardRowThird.add(WORD_HOME);
            
            rkm = createReplyKeyboard(keyboardRowFirst, keyboardRowSecond, keyboardRowThird);

        } else {
            buffer.append("Не дежуришь в офисе\n");
            keyboardRowFirst.add(WORD_ENABLE_DUTY);
            keyboardRowSecond.add(WORD_HOME);
            rkm = createReplyKeyboard(keyboardRowFirst, keyboardRowSecond);
        }
        return buffer.toString();
    }

    public ReplyKeyboardMarkup homeKeyboard() {
        keyboardRowFirst.add(WORD_SETTINGS);
        return createReplyKeyboard(keyboardRowFirst);
    }

    synchronized void sheduling(Employee e) {
        if (e.getCondition() != 2) {
            e.clearOfficeShifts();
            e.setCondition(2);
        }
        
        cleanKeyboard();
        
        for (String s : e.getOfficeShifts()) {
            keyboardRowFirst.add(s);
        }
        keyboardRowSecond.add(WORD_DONE);
        rkm = createReplyKeyboard(keyboardRowFirst, keyboardRowSecond);
        
        sendMessage(e.getChatId(), MSG_SHEDULING, rkm);
        
    }
}
